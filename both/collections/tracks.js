this.Tracks = new Mongo.Collection("tracks");

this.Tracks.userCanInsert = function(userId, doc) {
    return true;
}

this.Tracks.userCanUpdate = function(userId, doc) {
    return true;
}

this.Tracks.userCanRemove = function(userId, doc) {
    return true;
}

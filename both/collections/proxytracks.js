this.ProxyTracks = new Mongo.Collection("proxytracks");

this.ProxyTracks.userCanInsert = function(userId, doc) {
    return true;
}

this.ProxyTracks.userCanUpdate = function(userId, doc) {
    return true;
}

this.ProxyTracks.userCanRemove = function(userId, doc) {
    return true;
}

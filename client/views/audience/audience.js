Template.Audience.onRendered(function(){

    // Animate panel function
    $.fn['animatePanel'] = function() {

        var element = $(this);
        var effect = $(this).data('effect');
        var delay = $(this).data('delay');
        var child = $(this).data('child');

        // Set default values for attrs
        if(!effect) { effect = 'fadeIn'}
        if(!delay) { delay = 0.06 } else { delay = delay / 10 }
        if(!child) { child = '.row > div'} else {child = "." + child}

        //Set defaul values for start animation and delay
        var startAnimation = 0;
        var start = Math.abs(delay) + startAnimation;

        // Get all visible element and set opacity to 0
        var panel = element.find(child);
        panel.addClass('opacity-0');

        // Get all elements and add effect class
        panel = element.find(child);
        panel.addClass('stagger').addClass('animated-panel').addClass(effect);

        var panelsCount = panel.length + 10;
        var animateTime = (panelsCount * delay * 10000000) / 10;

        // Add delay for each child elements
        panel.each(function (i, elm) {
            start += delay;
            var rounded = Math.round(start * 10) / 10;
            $(elm).css('animation-delay', rounded + 's');
            // Remove opacity 0 after finish
            $(elm).removeClass('opacity-0');
        });

        // Clear animation after finish
        setTimeout(function(){
            $('.stagger').css('animation', '');
            $('.stagger').removeClass(effect).removeClass('animated-panel').removeClass('stagger');
        }, animateTime)
    };

    $('.animate-panel').animatePanel();

});



Template.Audience.rendered = function() {


};

Template.Audience.events({

    'click .closebox': function(event){
        event.preventDefault();
        var hpanel = $(event.target).closest('div.hpanel');
        hpanel.remove();
    },

    'click .run-tour-audience': function(){
        // Instance the tour
        var tourAudience = new Tour({
            backdrop: true,
            onShown: function(tourAudience) {

                // ISSUE    - https://github.com/sorich87/bootstrap-tour/issues/189
                // FIX      - https://github.com/sorich87/bootstrap-tour/issues/189#issuecomment-49007822

                // You have to write your used animated effect class
                // Standard animated class
                $('.animated').removeClass('fadeIn');
                // Animate class from animate-panel plugin
                $('.animated-panel').removeClass('fadeIn');

            },
            steps: [
                {
                    element: ".tour-1",
                    title: "Follower Database",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "top"

                },
                {
                    element: ".tour-2",
                    title: "Audience Growth",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "top"

                },
                {
                    element: ".tour-3",
                    title: "Most Active Profile",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "top"

                },
                {
                    element: ".tour-4",
                    title: "Most Active Chat Platform",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "top"

                },
                {
                    element: ".tour-5",
                    title: "List of Groups created",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "top"

                }
            ]});

        // Initialize the tour
        tourAudience.init();
        tourAudience.restart();
    }

});

Template.Audience.helpers({

});

var AudienceViewItems = function(cursor) {

};


Template.AudienceView.rendered = function() {
	pageSession.set("AudienceViewStyle", "table");

};

Template.AudienceView.events({

});

Template.AudienceView.helpers({

});


Template.AudienceViewTable.rendered = function() {

};

Template.AudienceViewTable.events({

});

Template.AudienceViewTable.helpers({

});


Template.AudienceViewTableItems.rendered = function() {

};

Template.AudienceViewTableItems.events({

});

Template.AudienceViewTableItems.helpers({

});
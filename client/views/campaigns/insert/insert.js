var pageSession = new ReactiveDict();

Template.CampaignsInsert.onRendered(function(){
    // Move modal to body
    // Fix Bootstrap backdrop issu with animation.css
    /*$('.modal').appendTo("body");

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('a[data-toggle="tab"]').removeClass('text-primary');
        $('a[data-toggle="tab"]').addClass('btn-muted');
        $(this).removeClass('text-muted');
        $(this).addClass('text-primary');
    });*/
	//document.getElementById("form-submit-button").onclick=submit;
});	

Template.CampaignsInsert.rendered = function() {

	/*$(window).load(function(){
        $('#campaignSetup').modal('show');
    });*/


	//$("[name='my-checkbox']").bootstrapSwitch();

};

Template.CampaignsInsert.events({
	"submit": function(e, t) {

		e.preventDefault();
		pageSession.set("campaignsInsertInsertFormInfoMessage", "");
		pageSession.set("campaignsInsertInsertFormErrorMessage", "");

		var self = this;

		//alert("form-submit-button"+document.getElementById("form-submit-button"));
		if (document.getElementById("form-submit-button") != null) {
			linkIndex = "1";
		}
		else if (document.getElementById("button2") != null) {
			linkIndex = "2";
		}
		else if (document.getElementById("button3") != null) {
			linkIndex = "3";
		}
		else if (document.getElementById("button5") != null) {
			linkIndex = "5";
		}else if (document.getElementById("button6") != null) {
			linkIndex = "6";
		}




		///???Router.go("campaigns", {});


		//alert("msg="+msg);
		if (linkIndex == "1") {


			document.getElementById("form-submit-button").setAttribute("id", "button2");
			document.getElementById("button2").setAttribute("name", "button2");

			//document.getElementById("note1t").classList.remove('text-dark');
			//document.getElementById("note1t").classList.add('text-primary');

			//document.getElementById("note1v").classList.remove('text-muted');
			//document.getElementById("note1v").classList.add('text-primary');
		} else if (linkIndex == "2") {

			document.getElementById("button2").setAttribute("id", "button3");
			document.getElementById("button3").setAttribute("name", "button3");

			//	document.getElementById("note2t").classList.remove('text-dark');
			//document.getElementById("note2t").classList.add('text-primary');

			//document.getElementById("note2v").classList.remove('text-muted');
			//document.getElementById("note2v").classList.add('text-primary');
		} else if (linkIndex == "3") {

			document.getElementById("button3").setAttribute("id", "button5");
			document.getElementById("button5").setAttribute("name", "button5");

			//document.getElementById("note3t").classList.remove('text-dark');
			//document.getElementById("note3t").classList.add('text-primary');

			//document.getElementById("note3v").classList.remove('text-muted');
			//document.getElementById("note3v").classList.add('text-primary');
		} else if (linkIndex == "5") {

			document.getElementById("button5").setAttribute("id", "button6");
			document.getElementById("button6").setAttribute("name", "button6");

			//document.getElementById("note5t").classList.remove('text-dark');
			//document.getElementById("note5t").classList.add('text-primary');

			//document.getElementById("note5v").classList.remove('text-muted');
			//document.getElementById("note5v").classList.add('text-primary');
		}else if (linkIndex == "6") {
			//	document.getElementById("note6t").classList.remove('text-dark');
			//	document.getElementById("note6t").classList.add('text-primary');

			//document.getElementById("note6v").classList.remove('text-muted');
			//document.getElementById("note6v").classList.add('text-primary');
			Router.go("campaigns", {});
		}


		//function submitAction(msg) {


		///???Router.go("campaigns", {});


		//alert("msg linkIndex="+linkIndex);
		if (linkIndex == "1") {

			var campaignsInsertInsertFormMode = "insert";
			if (!t.find("#form-cancel-button")) {
				switch (campaignsInsertInsertFormMode) {
					case "insert":
					{
						$(e.target)[0].reset();
					}
						;
						break;

					case "update":
					{
						var message = msg || "Saved.";
						pageSession.set("campaignsInsertInsertFormInfoMessage", message);
					}
						;
						break;
				}
			}


		}


		//}


		if (linkIndex == "1") {
			linkIndex = "2";
		}
		else if (linkIndex == "2") {
			linkIndex = "3";
		}
		else if (linkIndex == "3") {
			linkIndex = "5";
		}
		else if (linkIndex == "5") {
			linkIndex = "6";
		}
		//alert("link index=" + linkIndex);
		console.log("link index=" + linkIndex);
		//document.getElementById('noteclick' + linkIndex).click();


		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("campaignsInsertInsertFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {




				if( linkIndex=="2" ) {

					newId = Campaigns.insert(values, function (e) {
						if (e) errorAction(e); else submitAction();
					});
					//newId = Campaigns.save(values, function(e) { if(e) errorAction(e); else submitAction(); });
				}

				//if (typeof newId == 'undefined'){
				//
				//	newId = Campaigns.insert(values, function(e) { if(e) errorAction(e); else submitAction(); });
				//	console.log("Insert new...");
				//
				//
				//}else{
				//
				//	newId = Campaigns.update(values, function(e) { if(e) errorAction(e); else submitAction(); },{upsert:true});
				//	console.log("Update...");
				//
				//}
				//
				//console.log("newId="+newId);



			}
		);
		if( linkIndex!="6" )
			document.getElementById("form-submit-button").click();
		return false;
	}

	
});

Template.CampaignsInsert.helpers({
	
});


Template.CampaignsInsert.rendered = function() {
	

	pageSession.set("campaignsInsertInsertFormInfoMessage", "");
	pageSession.set("campaignsInsertInsertFormErrorMessage", "");

	$('.datetimepicker').datetimepicker();


	/*$(window).load(function(){
        $('#myModal').modal('show');
    });*/


	//$("[name='my-checkbox']").bootstrapSwitch();

	var elemActive = document.querySelector('.js-switchActive');
	var initActive = new Switchery(elemActive);


	var elemFB = document.querySelector('.js-switchFB');
	var initFB = new Switchery(elemFB);

	var elemKik = document.querySelector('.js-switchKik');
	var initKik = new Switchery(elemKik);

	var elemTe = document.querySelector('.js-switchTe');
	var initTe = new Switchery(elemTe);
	
	var elemSl = document.querySelector('.js-switchSl');
	var initSl = new Switchery(elemSl);	

	var elemSk = document.querySelector('.js-switchSk');
	var initSk = new Switchery(elemSk);	

	var elemKa = document.querySelector('.js-switchKa');
	var initKa = new Switchery(elemKa);	

	var elemVi = document.querySelector('.js-switchVi');
	var initVi = new Switchery(elemVi);	

	var elemWha = document.querySelector('.js-switchWha');
	var initWha = new Switchery(elemWha);

	var elemNi = document.querySelector('.js-switchNi');
	var initNi = new Switchery(elemNi);	

	var elemQq = document.querySelector('.js-switchQq');
	var initQq = new Switchery(elemQq);	

	var elemLi = document.querySelector('.js-switchLi');
	var initLi = new Switchery(elemLi);

	var elemWe = document.querySelector('.js-switchWe');
	var initWe = new Switchery(elemWe);



	

	var elembc1 = document.querySelector('.js-bc-welcome');
	var initbc1 = new Switchery(elembc1);

	var elembc2 = document.querySelector('.js-bc-newsmenu');
	var initbc2 = new Switchery(elembc2);

	var elembc3 = document.querySelector('.js-bc-articleopt');
	var initbc3 = new Switchery(elembc3);


	/*var elemTa = document.querySelector('.js-switchTa');
	var initTa = new Switchery(elemTa);		

	var elemSn = document.querySelector('.js-switchSn');
	var initSn = new Switchery(elemSn);



	var elemHi = document.querySelector('.js-switchHi');
	var initHi = new Switchery(elemHi);


	var elemIm = document.querySelector('.js-switchIm');
	var initIm = new Switchery(elemIm);		*/			


	//$(".input-group.date").each(function() {
	//	var format = $(this).find("input[type='text']").attr("data-format");
    //
	//	if(format) {
	//		format = format.toLowerCase();
	//	}
	//	else {
	//		format = "mm/dd/yyyy";
	//	}
    //
	//	$(this).datepicker({
	//		autoclose: true,
	//		todayHighlight: true,
	//		todayBtn: true,
	//		forceParse: false,
	//		keyboardNavigation: false,
	//		format: format
	//	});
	//});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

var linkIndex="1";

Template.CampaignsInsert.events({


    /*'click .next': function(event){
        var nextId = $(event.target).parents('.tab-pane').next().attr("id");
        $('[href=#'+nextId+']').tab('show');
    },
    'click .prev': function(event){
        var prevId = $(event.target).parents('.tab-pane').prev().attr("id");
        $('[href=#'+prevId+']').tab('show');
    },*/




	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		Router.go("campaigns", {});
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	},
	"click #insertKey" : function(e,t) {

		event.preventDefault();

		var tkey = document.getElementById('keyword_key').value;
		var tvalue = document.getElementById('keyword_value').value;

		var wrapperKey = {};
		var key = tkey;
		wrapperKey['keyword.' +key] = tvalue;

		Campaigns.update({  _id: t.data.campaign._id }, { $set: wrapperKey },function(e) {  console.log("Success save keywords "); });


	},
	"click #removeKey" : function(e,t) {
		event.preventDefault();
		var tkey = document.getElementById('keyword_key').value;
		var tvalue = document.getElementById('keyword_value').value;

		var wrapperKey = {};
		var key = tkey;
		wrapperKey['keyword.' +key] = 'empty';

		///???Campaigns.remove({ _id: me._id });

		Campaigns.update({  _id: t.data.campaign._id }, { $set: wrapperKey },function(e) {  console.log("Success remove keywords "); });
	},
	"click #insertKeyRSS" : function(e,t) {

		event.preventDefault();

		var tkey = document.getElementById('rss_key').value;
		var tvalue = document.getElementById('rss_value').value;

		var wrapperKey = {};
		var key = tkey;
		wrapperKey['rss.' +key] = tvalue;

		Campaigns.update({  _id: t.data.campaign._id }, { $set: wrapperKey },function(e) {  console.log("Success save rss "); });


	},
	"click #removeKeyRSS" : function(e,t) {
		event.preventDefault();
		var tkey = document.getElementById('rss_key').value;
		var tvalue = document.getElementById('rss_value').value;

		var wrapperKey = {};
		var key = tkey;
		wrapperKey['rss.' +key] = 'empty';


		Campaigns.update({  _id: t.data.campaign._id }, { $set: wrapperKey },function(e) {  console.log("Success remove rss "); });
	}

	
});

Template.CampaignsInsert.helpers({
	"infoMessage": function() {
		return pageSession.get("campaignsInsertInsertFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("campaignsInsertInsertFormErrorMessage");
	},
	keyword: function(){
		return _.map(this.keyword, function(value, key){
			return {
				key: key,
				value: value
			};
		});
	},
	rss: function(){
		return _.map(this.rss, function(value, key){
			return {
				key: key,
				value: value
			};
		});
	}
	
});

Template.CampaignsInsert.camp = function() {

	//console.log("Campaigns: "+Campaigns.find());

	return Campaigns.find();
}

Template.registerHelper('compare', function(v1, v2) {
	if (typeof v1 === 'object' && typeof v2 === 'object') {
		return _.isEqual(v1, v2); // do a object comparison
	} else {
		return v1 === v2;
	}
});

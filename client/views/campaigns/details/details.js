var pageSession = new ReactiveDict();

var labelsDay = function () {
    return ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"]
}
var labelsWeek = function () {
    var array = [];
    var currentDate = new Date();
    var date = new Date();
    var currentDay = currentDate.getDay();
    var dateOfSunday = currentDate.getDate() - currentDay;


    array.push("Sunday " + new Date(date.setDate(dateOfSunday)).getDate() + ordinalNumber(new Date(date.setDate(dateOfSunday)).getDate()));
    date=new Date();
    array.push("Monday " + new Date(date.setDate(dateOfSunday + 1)).getDate() + ordinalNumber(new Date(date.setDate(dateOfSunday + 1)).getDate()));
    date=new Date();
    array.push("Tuesday " + new Date(date.setDate(dateOfSunday + 2)).getDate() + ordinalNumber(new Date(date.setDate(dateOfSunday + 2)).getDate()));
    date=new Date();
    array.push("Wednesday " + new Date(date.setDate(dateOfSunday + 3)).getDate() + ordinalNumber(new Date(date.setDate(dateOfSunday + 3)).getDate()));
    date=new Date();
    array.push("Thursday " + new Date(date.setDate(dateOfSunday + 4)).getDate() + ordinalNumber(new Date(date.setDate(dateOfSunday + 4)).getDate()));
    date=new Date();
    array.push("Friday " + new Date(date.setDate(dateOfSunday + 5)).getDate() + ordinalNumber(new Date(date.setDate(dateOfSunday + 5)).getDate()));
    date=new Date();
    array.push("Saturday " + new Date(date.setDate(dateOfSunday + 6)).getDate() + ordinalNumber(new Date(date.setDate(dateOfSunday + 6)).getDate()));

    return array;
}
var labelsMonth = function () {
    var array = [];
    var currentDate = new Date();
    var numberOfDays = new Date(currentDate.getFullYear(), currentDate.getMonth()+1, 0).getDate();

    for (var i=1; i<=numberOfDays; i++)
        array.push(i);

    return array;
}
var ordinalNumber = function(day) {
    if (day==1 || day==21 || day==31)
        return "st";
    else if(day==2 || day==22)
        return "nd";
    else if(day==2 || day==22)
        return "rd";
    else
        return "th";
}



var Views = function(times, thisCampaignName){

    var usersViews= Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}},
        {campaignName: thisCampaignName }]}).fetch().map(function (x) {
        return x.repeatView;
    });
    var usersViews2= Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}},
        {campaignName: thisCampaignName }]}).fetch().map(function (x) {
        return x.repeatView;
    });
    var sum=0;
    for(var i=0; i<usersViews.length; i++){
        sum+=parseInt(usersViews[i])+1;
    }
    for(var i=0; i<usersViews2.length; i++){
        sum+=parseInt(usersViews2[i])+1;
    }
    return sum;
};

var Interactions = function(times, thisCampaignName) {
    var messagesArr = Tracks.find({
        $and: [{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}},
            {campaignName: thisCampaignName}]
    }).fetch().map(function (x) {
        return x.message;
    });
    var messagesArr2 = Tracks.find({
        $and: [{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}},
            {campaignName: thisCampaignName}]
    }).fetch().map(function (x) {
        return x.message;
    });
    var total=0;
    for (var i = 0; i < messagesArr.length; i++) {
        total += messagesArr[i].split(",").length;
    }
    for (var i = 0; i < messagesArr2.length; i++) {
        total += messagesArr2[i].split(",").length;
    }


    return total;
};

var UniqueUsers = function(times, thisCampaignName ) {
    var a=Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}},
        {campaignName: thisCampaignName }]}).count();
    var b=Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}},
        {campaignName: thisCampaignName }]}).count();
    return a+b;

}

var RepeatedUsers = function(times, thisCampaignName){

    var a= Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}},
        {repeatView: {$gt: "0"}},
        {campaignName: thisCampaignName }]}).count();
    var b= Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}},
        {repeatView: {$gt: "0"}},
        {campaignName: thisCampaignName }]}).count();
    //  console.log("start"+start+"end"+end+"a"+a+"b"+b)
    return a+b;
};

var TimeSpentPerUser = function(times, thisCampaignName) {

    var timeArray = Tracks.find({
        $and: [{createdAt: {$gte: times.start, $lt: times.end}},
            {createdAt: {$type: 9}},
            {modifiedAt: {$type: 9}},
            {campaignName: thisCampaignName}]
    }).fetch().map(function (x) {
        return ({"createdAt":x.createdAt, "modifiedAt":x.modifiedAt});
    });

    var counter = 0;
    var aggregator = 0;
    for (var i = 0; i < timeArray.length; i++) {
        if (timeArray[i]!=undefined && timeArray[i].createdAt != undefined && timeArray[i].modifiedAt != undefined) {
            if (timeArray[i].modifiedAt.getHours() == timeArray[i].createdAt.getHours()) {
                var mintues = timeArray[i].modifiedAt.getMinutes() - timeArray[i].createdAt.getMinutes();
                aggregator += mintues;
                counter++;
            }
        }
    }

    if(aggregator==0&&counter==0)
        return 0;

    var result = aggregator/counter;
    result = result.toFixed(2);

    return result;
};

var RepeatUsePerUser= function (times, thisCampaignName) {
    var totalViews = Views(times, thisCampaignName);
    var totalUniqueUsers = UniqueUsers(times, thisCampaignName);

    if(totalViews==0&&totalUniqueUsers==0)
        return 0;

    var result = totalViews/totalUniqueUsers;
    result = result.toFixed(2);

    return result;
};

var setTimeArray=function(time, timeType) {
    if (timeType == "Day") {

        var start = new Date(), end = new Date();
        start.setHours(time, 0, 0, 0);
        end.setHours(time, 59, 59, 999);
        return {start: start, end: end};
    }
    if (timeType == "Week") {
        var day=time.replace(/[^0-9]/g,'');

        var start = new Date(), end = new Date();
        start.setDate(day);
        end.setDate(day);
        start.setHours(0, 0, 0, 0);
        end.setHours(23, 59, 59, 999);
        return {start: start, end: end};
    }
    if (timeType == "Month") {

        var start = new Date(), end = new Date();
        start.setDate(time);
        end.setDate(time);
        start.setHours(0, 0, 0, 0);
        end.setHours(23, 59, 59, 999);
        return {start: start, end: end};
    }


}

var DataArray= function(fnName,timeType, cn){
    var labels=eval("labels"+timeType)();
    var data=[];
    for(var i=0; i<labels.length; i++){
        var element=eval(fnName)(setTimeArray(labels[i],timeType),cn);
        data.push(element);
    }
    return data;
}



Template.CampaignsDetails.rendered = function() {
    var start = new Date();
    start.setHours(0,0,0,0);

    var end = new Date();
    end.setHours(23,59,59,999);

    var cn = document.getElementById('cn').value;

    var dayViewsData= {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("Views","Day", cn)

            }
        ]
    };

    var dayInteractionsData = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("Interactions","Day", cn)

            }
        ]
    };

    var dayUniqueUsersData = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("UniqueUsers","Day", cn)

            }
        ]
    };

    var dayRepeatedUsersData = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatedUsers","Day", cn)

            }
        ]
    };

    var dayTimeSpentPerUserData = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("TimeSpentPerUser","Day", cn)

            }
        ]
    };

    var dayRepeatUsePerUserData = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatUsePerUser","Day", cn)

            }
        ]
    };

    var weekViewsData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("Views","Week", cn)

            }
        ]
    };

    var weekInteractionsData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("Interactions","Week", cn)

            }
        ]
    };

    var weekUniqueUsersData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("UniqueUsers","Week", cn)

            }
        ]
    };

    var weekRepeatedUsersData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatedUsers","Week", cn)

            }
        ]
    };

    var weekTimeSpentPerUserData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("TimeSpentPerUser","Week", cn)
            }
        ]
    };

    var weekRepeatUsePerUserData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatUsePerUser","Week", cn)

            }
        ]
    };

    var monthViewsData = {
        labels: labelsMonth(),
        datasets: [
            {
               label: "Line 1",
               fillColor: "rgba(159,159,159,0.5)",
               strokeColor: "rgba(159,159,159,1)",
               pointColor: "rgba(159,159,159,1)",
               pointStrokeColor: "#fff",
               pointHighlightFill: "#fff",
               pointHighlightStroke: "rgba(159,159,159,1)",
               data: DataArray("Views","Month", cn)

            }
        ]
    };

    var monthInteractionsData = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("Interactions","Month", cn)

            }
        ]
    };

    var monthUniqueUsersData = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("UniqueUsers","Month", cn)

            }
        ]
    };

    var monthRepeatedUsersData = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatedUsers","Month", cn)
            }
        ]
    };

    var monthTimeSpentPerUserData = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("TimeSpentPerUser","Month", cn)

            }
        ]
    };

    var monthRepeatUsePerUserData = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatUsePerUser","Month", cn)

            }
        ]
    };

    var lineOptions = {
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 1,
        datasetFill : true,
        responsive: true
    };

    var ctx = document.getElementById("lineOptions-by-chat-platform1").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayViewsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform2").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayInteractionsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform3").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayUniqueUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform4").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayRepeatedUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform5").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayTimeSpentPerUserData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform6").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayRepeatUsePerUserData, lineOptions);

    var ctx = document.getElementById("lineOptions-by-chat-platform7").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthViewsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform8").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthInteractionsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform9").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthUniqueUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform10").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthRepeatedUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform11").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthTimeSpentPerUserData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform12").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthRepeatUsePerUserData, lineOptions);

    var ctx = document.getElementById("lineOptions-by-chat-platform13").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekViewsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform14").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekInteractionsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform15").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekUniqueUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform16").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekRepeatedUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform17").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekTimeSpentPerUserData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform18").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekRepeatUsePerUserData, lineOptions);



    // Options for Response Rate
    var usersWhoFinishedChart = [
        {
            value: 55404,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Finished"
        },
        {
            value: 90396,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Not Finished"
        }
    ];





    var doughnutData = [
        {
            value: 36450,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Key Clicks"
        },
        {
            value: 109350,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Key Views"
        }
    ];

    var doughnutData1 = [
        {
            value: 37597,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Used"
        },
        {
            value: 49838,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Not Used"
        }
    ];

    var doughnutData2 = [
        {
            value: 1127910,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Used"
        },
        {
            value: 1495140,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Not Used"
        }
    ];

    var doughnutData3 = [
        {
            value: 263179,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Used"
        },
        {
            value: 348866,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Not Used"
        }
    ];


    var doughnutOptions = {
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 0, // This is 0 for Pie charts
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true
    };

    // var ctx = document.getElementById("usersWhoFinishedChartDaily").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(usersWhoFinishedChart, doughnutOptions);
    // var ctx = document.getElementById("usersWhoFinishedChartWeekly").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(usersWhoFinishedChart, doughnutOptions);
    // var ctx = document.getElementById("usersWhoFinishedChartMonthly").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(usersWhoFinishedChart, doughnutOptions);
    //
    // var ctx = document.getElementById("ConversionRateChartDaily").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
    // var ctx = document.getElementById("ConversionRateChartWeekly").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
    // var ctx = document.getElementById("ConversionRateChartMonthly").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
    //
    var ctx = document.getElementById("doughnutChart3d").getContext("2d");
    var myNewChart = new Chart(ctx).Doughnut(doughnutData1, doughnutOptions);
   var ctx = document.getElementById("doughnutChart3w").getContext("2d");
   var myNewChart = new Chart(ctx).Doughnut(doughnutData3, doughnutOptions);
    var ctx = document.getElementById("doughnutChart3m").getContext("2d");
    var myNewChart = new Chart(ctx).Doughnut(doughnutData2, doughnutOptions);


   // Options for gender and Age
    var barOptions = {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        barShowStroke : true,
        barStrokeWidth : 1,
        barValueSpacing : 7,
        barDatasetSpacing : 1,
        responsive:true
    };

    var barData2 = {
        labels: ["Tell me more", "Show me more title", "Return to main menu", "etc..."],
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [80, 35, 97, 15]
            }
        ]
    };
    var barData1 = {
        labels: ["Drama", "Comedy", "Documentary", "Surprise Me!"],
        datasets: [
            {
                label: "Usage",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [35, 75, 60, 90]
            },
            {
                label: "Conversion",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [20, 47, 35, 55]
            }
        ]
    };

    var barData2w = {
        labels: ["Tell me more", "Show me more title", "Return to main menu", "etc..."],
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [560, 245, 679, 105]
            }
        ]
    };
    var barData1w = {
        labels: ["Drama", "Comedy", "Documentary", "Surprise Me!"],
        datasets: [
            {
                label: "Usage",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [245, 525, 420, 630]
            },
            {
                label: "Conversion",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [140, 329, 245, 385]
            }
        ]
    };


    var barData2m = {
        labels: ["Tell me more", "Show me more title", "Return to main menu", "etc..."],
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [2400, 1050, 2910, 450]
            }
        ]
    };
    var barData1m = {
        labels: ["Drama", "Comedy", "Documentary", "Surprise Me!"],
        datasets: [
            {
                label: "Usage",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [1050, 2250, 1800, 2700]
            },
            {
                label: "Conversion",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [600, 1410, 1050, 1650]
            }
        ]
    };
    var barData3 = {
        labels: ["-12", "13-17", "18-24", "25-34", "35-44", "45-54", "55-64","65+"],
        datasets: [
            {
                label: "Male",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [550, 1900, 2800, 3400,2750, 2136, 990,600]
            },
            {
                label: "Female",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [450, 1800, 2550, 2500, 2700, 1700, 800, 400]
            }
        ]
    };
    var barData3m = {
        labels: ["-12", "13-17", "18-24", "25-34", "35-44", "45-54", "55-64","65+"],
        datasets: [
            {
                label: "Male",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [16500,57000,84000,102000,82500,64080,29700,18000]
            },
            {
                label: "Female",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [13500,54000,76500,75000,81000,51000,24000,12000]
            }
        ]
    };
    var barData3w = {
        labels: ["-12", "13-17", "18-24", "25-34", "35-44", "45-54", "55-64","65+"],
        datasets: [
            {
                label: "Male",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [3850,13300,19600,23800,19250,14952,6930,4200]
            },
            {
                label: "Female",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [3150,12600,17850,17500,18900,11900,5600,2800]
            }
        ]
    };

    var ctx = document.getElementById("barOptions-gender-age1d").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData1, barOptions);

    var ctx = document.getElementById("barOptions-gender-age2d").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData2, barOptions);
    var ctx = document.getElementById("barOptions-gender-age1w").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData1w, barOptions);

    var ctx = document.getElementById("barOptions-gender-age2w").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData2w, barOptions);

    var ctx = document.getElementById("barOptions-gender-age1m").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData1m, barOptions);

    var ctx = document.getElementById("barOptions-gender-age2m").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData2m, barOptions);

    var ctx = document.getElementById("barOptions-gender-ageDaily").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData3, barOptions);
    var ctx = document.getElementById("barOptions-gender-ageWeekly").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData3w, barOptions);
    var ctx = document.getElementById("barOptions-gender-ageMonthly").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData3m, barOptions);


    document.getElementById('wg6').click();
    document.getElementById('wg5').click();
    document.getElementById('wg4').click();
    document.getElementById('wg3').click();
    document.getElementById('wg2').click();
    document.getElementById('wg1').click();
    document.getElementById('mg6').click();
    document.getElementById('mg5').click();
    document.getElementById('mg4').click();
    document.getElementById('mg3').click();
    document.getElementById('mg2').click();
    document.getElementById('mg1').click();
    document.getElementById('dg6').click();
    document.getElementById('dg5').click();
    document.getElementById('dg4').click();
    document.getElementById('dg3').click();
    document.getElementById('dg2').click();
    document.getElementById('dg1').click();

    document.getElementById('dailyg').className = "tab-pane active";
    document.getElementById('weeklyg').className = "tab-pane";
    document.getElementById('monthlyg').className = "tab-pane";

    /*

  //  $('body').jpreLoader();
    console.log("average Interactions Per User " + averageInteractionsPerUser(cn));
    console.log("Users Who Clicked On Tv " + usersWhoClickedOnTv(cn));
    console.log("Users Who Clicked On Tv Percentage " + usersWhoClickedOnTvPercentage(cn));
    console.log("Users Who Clicked On Movie " + usersWhoClickedOnMovie(cn));
    console.log("Users Who Clicked On Movie Percentage " + usersWhoClickedOnMoviePercentage(cn));
    console.log("Users Who Clicked On Movie Comedy " + usersWhoClickedOnMovieComedy(cn));
    console.log("Users Who Clicked On Movie Comedy Percentage " + usersWhoClickedOnMovieComedyPercentage(cn));
    console.log("Users Who Clicked On Movie Drama " + usersWhoClickedOnMovieDrama(cn));
    console.log("Users Who Clicked On Movie Drama Percentage " + usersWhoClickedOnMovieDramaPercentage(cn));
    console.log("Users Who Clicked On Movie Documentary " + usersWhoClickedOnMovieDocumentary(cn));
    console.log("Users Who Clicked On Movie Documentary Percentage " + usersWhoClickedOnMovieDocumentaryPercentage(cn));
    console.log("Users Who Clicked On Movie Surprise " + usersWhoClickedOnMovieSurprise(cn));
    console.log("Users Who Clicked On Movie Surprise Percentage " + usersWhoClickedOnMovieSurprisePercentage(cn));
    console.log("Users Who Clicked On Tv Comedy " + usersWhoClickedOnTvComedy(cn));
    console.log("Users Who Clicked On Tv Comedy Percentage " + usersWhoClickedOnTvComedyPercentage(cn));
    console.log("Users Who Clicked On Tv Drama " + usersWhoClickedOnTvDrama(cn));
    console.log("Users Who Clicked On Tv Drama Percentage " + usersWhoClickedOnTvDramaPercentage(cn));
    console.log("Users Who Clicked On Tv Documentary " + usersWhoClickedOnTvDocumentary(cn));
    console.log("Users Who Clicked On Tv Documentary Percentage " + usersWhoClickedOnTvDocumentaryPercentage(cn));
    console.log("Users Who Clicked On Tv Surprise " + usersWhoClickedOnTvSurprise(cn));
    console.log("Users Who Clicked On Tv Surprise Percentage " + usersWhoClickedOnTvSurprisePercentage(cn));
    console.log(mostPopularTitles(cn));


    */
};

Template.CampaignsDetails.events({

    "click #btnDaily": function(e, t) {
        e.preventDefault();

        document.getElementById('dailyg').className = "tab-pane active";
       document.getElementById('weeklyg').className = "tab-pane";
        document.getElementById('monthlyg').className = "tab-pane";

        //console.log("daily view");
    },
    "click #btnWeekly": function(e, t) {
        e.preventDefault();

      document.getElementById('dailyg').className = "tab-pane";
        document.getElementById('weeklyg').className = "tab-pane active";
        document.getElementById('monthlyg').className = "tab-pane";

        //console.log("weekly view");
    },
    "click #btnMonthly": function(e, t) {
        e.preventDefault();

       document.getElementById('dailyg').className = "tab-pane";
       document.getElementById('weeklyg').className = "tab-pane";
        document.getElementById('monthlyg').className = "tab-pane active";

        //console.log("monthly view");
    }
});
var setTime=function(time){
    var start=new Date(), end=new Date();
    if(time=="Today"){
        start.setHours(0,0,0,0);
        end.setHours(23,59,59,999);
    }
    if(time=="Yesterday"){
        start.setDate(start.getDate() - 1);
        start.setHours(0,0,0,0);
        end.setDate(end.getDate() - 1);
        end.setHours(23,59,59,999);
    }
    if(time=="ThisWeek"){
        var daytoset=1,distance; // 1 is Monday
        var currentDay = start.getDay();
        if(currentDay==0)//today is sunday
            distance = -6;
        else
            distance= daytoset - currentDay;

        start.setDate(start.getDate() + distance);
        start.setHours(0,0,0,0);

        end.setHours(23,59,59,999);
    }
    if(time=="LastWeek"){
        var daytoset=1,distance; // 1 is Monday
        var currentDay = start.getDay();
        if(currentDay==0)//today is sunday
            distance = -6;
        else
            distance= daytoset - currentDay;

        start.setDate(start.getDate() + distance-7);
        start.setHours(0,0,0,0);
        end.setDate(start.getDate() + distance-1);
        end.setHours(23,59,59,999);
    }
    if(time=="ThisMonth"){

        start.setDate(1);
        start.setHours(0,0,0,0);

        end.setHours(23,59,59,999);
    }
    if(time=="LastMonth"){

        start.setDate(1);
        start.setHours(0,0,0,0);
        start.setMonth(start.getMonth() - 1);

        end=new Date(end.getFullYear(), end.getMonth(), 0);
        end.setHours(23,59,59,999);
    }
    if(time=="All"){
        start.setYear(2015);
        end.setHours(23,59,59,999);
    }

    return {start: start, end:end};
}
Template.CampaignsDetails.helpers({
    "textAttr": function (infoType, nowTime, lastTime) {
        var nowTimes=setTime(nowTime);
        var lastTimes=setTime(lastTime);
        if(eval(infoType)(lastTimes, this.campaign.name)==0)
            return {
                class: 'text-muted'

            };
        if(eval(infoType)(nowTimes, this.campaign.name) > eval(infoType)(lastTimes, this.campaign.name)) {
            return {
                class: 'text-success'
            };
        }
        else
        {
            return {
                class: 'text-danger'
            };
        }
    },
    "caretAttr": function (infoType, nowTime, lastTime) {
        var nowTimes=setTime(nowTime);
        var lastTimes=setTime(lastTime);
        if(eval(infoType)(lastTimes, this.campaign.name)==0)
            return {

            };
        if(eval(infoType)(nowTimes, this.campaign.name) > eval(infoType)(lastTimes, this.campaign.name)) {
            return {
                class: 'fa fa-caret-up'
            };
        }
        else
        {
            return {
                class: 'fa fa-caret-down'
            };
        }
     
    },
    "Precentage": function(fnName,time) {

        var times=setTime(time);
        var now =  eval(fnName)(times, this.campaign.name);
        if(time== "Today")
            time="Yesterday";
        if(time== "ThisMonth")
            time="LastMonth";
        if(time== "ThisWeek")
            time="LastWeek";

        times=setTime(time);
        var last = eval(fnName)(times, this.campaign.name);

        if(last == 0)
            return "Not available"
        if(now-last==0&&last==0)
            return 0;

        var result = (now-last)/last * 100;
        result = result.toFixed(2)+"%";

        return result;
    },
    "Views": function(time) {
        var times=setTime(time);
        return Views(times, this.campaign.name);
    },
    "Interactions": function(time) {
        var times=setTime(time);
        return Interactions(times, this.campaign.name);
    },

    "RepeatedUsers": function(time) {
        var times=setTime(time);
        return RepeatedUsers(times, this.campaign.name);
    },
    "UniqueUsers": function(time) {
        var times=setTime(time);
        return UniqueUsers(times, this.campaign.name);
    },
    "TimeSpentPerUser": function(time) {
        var times=setTime(time);
        return TimeSpentPerUser(times, this.campaign.name);
    },
    "RepeatUsePerUser": function(time) {
        var times=setTime(time);
        return RepeatUsePerUser(times, this.campaign.name);
    }

});

Template.CampaignsDetails.userParticipation = function() {
    return Tracks.find({reactive: false}).count();
};

Template.CampaignsDetailsDetailsForm.rendered = function() {
	

	pageSession.set("campaignsDetailsDetailsFormInfoMessage", "");
	pageSession.set("campaignsDetailsDetailsFormErrorMessage", "");

	$('.datetimepicker').datetimepicker();
	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
    var titles = [];

    var map = AmCharts.makeChart( "world-mapday", {
        "type": "map",
        "theme": "light",
        "colorSteps": 10,
        "dataProvider": {
            "map": "worldLow",
            "getAreasFromMap": true,
            "zoomLevel": 0.9,
            "areas": []
        },
        "areasSettings": {
            "autoZoom": true,
            "balloonText": "[[title]]: <strong>[[value]]</strong>"
        },
        "valueLegend": {
            "right": 10,
            "minValue": "little",
            "maxValue": "a lot!"
        },
        "zoomControl": {
            "minZoomLevel": 0.9
        },
        "titles": titles,
        "listeners":[{"event":"init", "method":updateHeatmap}]
    });
    var map = AmCharts.makeChart( "world-mapweek", {
        "type": "map",
        "theme": "light",
        "colorSteps": 10,
        "dataProvider": {
            "map": "worldLow",
            "getAreasFromMap": true,
            "zoomLevel": 0.9,
            "areas": []
        },
        "areasSettings": {
            "autoZoom": true,
            "balloonText": "[[title]]: <strong>[[value]]</strong>"
        },
        "valueLegend": {
            "right": 10,
            "minValue": "little",
            "maxValue": "a lot!"
        },
        "zoomControl": {
            "minZoomLevel": 0.9
        },
        "titles": titles,
        "listeners":[{"event":"init", "method":updateHeatmap}]
    }); var map = AmCharts.makeChart( "world-mapmonth", {
        "type": "map",
        "theme": "light",
        "colorSteps": 10,
        "dataProvider": {
            "map": "worldLow",
            "getAreasFromMap": true,
            "zoomLevel": 0.9,
            "areas": []
        },
        "areasSettings": {
            "autoZoom": true,
            "balloonText": "[[title]]: <strong>[[value]]</strong>"
        },
        "valueLegend": {
            "right": 10,
            "minValue": "little",
            "maxValue": "a lot!"
        },
        "zoomControl": {
            "minZoomLevel": 0.9
        },
        "titles": titles,
        "listeners":[{"event":"init", "method":updateHeatmap}]
    });
    function updateHeatmap(event) {
        var map = event.chart;
        if ( map.dataGenerated )
            return;
        if ( map.dataProvider.areas.length === 0 ) {
            setTimeout( updateHeatmap, 100 );
            return;
        }
        for ( var i = 0; i < map.dataProvider.areas.length; i++ ) {
            map.dataProvider.areas[ i ].value=0;
        }
        for ( var i = 0; i < mapdataarr.length; i++ ) {
            var obj = _.find(map.dataProvider.areas, function(obj) { return obj.title==mapdataarr[i].name });
            obj.value=mapdataarr[i].value;
        }
        // for ( var i = 0; i < map.dataProvider.areas.length; i++ ) {
        //       console.log (map.dataProvider.areas[ i ].title);
        //    }
        map.dataGenerated = true;
        map.validateNow();
    }


};

Template.CampaignsDetailsDetailsForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("campaignsDetailsDetailsFormInfoMessage", "");
		pageSession.set("campaignsDetailsDetailsFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var campaignsDetailsDetailsFormMode = "read_only";
			if(!t.find("#form-cancel-button")) {
				switch(campaignsDetailsDetailsFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("campaignsDetailsDetailsFormInfoMessage", message);
					}; break;
				}
			}

			/*SUBMIT_REDIRECT*/
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("campaignsDetailsDetailsFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				

				
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		/*CANCEL_REDIRECT*/
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		Router.go("campaigns", {});
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		Router.go("campaigns", {});
	}

	
});

Template.CampaignsDetailsDetailsForm.helpers({
	"infoMessage": function() {
		return pageSession.get("campaignsDetailsDetailsFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("campaignsDetailsDetailsFormErrorMessage");
	},
    "graphDate": function(dateType){

        switch (dateType) {
            case "Day":
                var today=new Date();
                var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                return today.toLocaleString("en-US",options);

                break;
            case "Week":
                var currentDate = new Date();
                var currentDay = currentDate.getDay();
                var dateOfSunday = currentDate.getDate() - currentDay;
                var sunday=new Date();
                sunday.setDate(dateOfSunday);
                var saterday=new Date();
                saterday.setDate(dateOfSunday+6);
                var options = {year: 'numeric', month: 'long', day: 'numeric' };
                return sunday.toLocaleString("en-US",options)+" to "+saterday.toLocaleString("en-US",options);
                break;
            case "Month":
                var currentDate=new Date();
                var options = {year: 'numeric', month: 'long' };
                return currentDate.toLocaleString("en-US",options);
                break;
        }
        return dateType;

    },
    "ProxyTrack_campaignName": function() {
        var c = ProxyTracks.findOne({sectionName: "appmedia"});
        return c.campaignName;
    },
    "ProxyTrack_url": function() {
        var c = ProxyTracks.findOne({sectionName: "appmedia"});
        return c.url;
    },
    "ProxyTrack_msg": function() {
        var c = ProxyTracks.findOne({sectionName: "appmedia"});
        return c.message;
    },
    "ProxyTrack_type": function() {
        var c = ProxyTracks.findOne({sectionName: "appmedia"});
        return c.type;
    },
    "ProxyTrack_platform": function() {
        var c = ProxyTracks.findOne({sectionName: "appmedia"});
        return c.platform;
    },
	
});



var mapdataarr=[{name:"Russia", value:5},
    {name:"United States", value:15},
    {name:"Thailand", value:10},
    {name:"Australia", value:20},
    {name:"Sweden", value:50},
    {name:"Iceland", value:15},
    {name:"Guatemala", value:10},
    {name:"Kenya", value:20},
    {name:"Iran", value:10},
    {name:"Mali", value:40},
    {name:"Brazil", value:18}];



var averageInteractionsPerUser = function(thisCampaignName) {
    var interactions = Interactions("All", thisCampaignName);
    var users = UniqueUsers("All", thisCampaignName);

    if (interactions==0 && users==0)
        return 0;

    return (interactions /users).toFixed(2);
}

var usersWhoClicked = function (thisCampaignName, name) {
    var messagesArr = Tracks.find({campaignName: thisCampaignName}).fetch().map(function (x) {
        return x.message;
    });
    var count=0;
    for(var i = 0; i < messagesArr.length; i++) {
        if(messagesArr[i].indexOf(name) > -1)
            count++;
    }

    return count;
}

var usersWhoClickedOnTv = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "tvshow");
}

var usersWhoClickedOnTvPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnTv(thisCampaignName) /(usersWhoClickedOnTv(thisCampaignName)+usersWhoClickedOnMovie(thisCampaignName)) * 100).toFixed(2);
}

var usersWhoClickedOnMovie = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "movie");
}


var usersWhoClickedOnMoviePercentage = function (thisCampaignName) {
    return (100-usersWhoClickedOnTvPercentage(thisCampaignName)).toFixed(2);
}

var usersWhoClickedOnMovieCategory = function (thisCampaignName) {
    return usersWhoClickedOnMovieComedy(thisCampaignName)+usersWhoClickedOnMovieDrama(thisCampaignName)+usersWhoClickedOnMovieDocumentary(thisCampaignName)+usersWhoClickedOnMovieSurprise(thisCampaignName);
}

var usersWhoClickedOnMovieComedy = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "movie,Comedy");
}

var usersWhoClickedOnMovieComedyPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnMovieComedy(thisCampaignName) / usersWhoClickedOnMovieCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnMovieDrama = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "movie,Drama");
}

var usersWhoClickedOnMovieDramaPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnMovieDrama(thisCampaignName) / usersWhoClickedOnMovieCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnMovieDocumentary = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "movie,Documentary");
}

var usersWhoClickedOnMovieDocumentaryPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnMovieDocumentary(thisCampaignName) / usersWhoClickedOnMovieCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnMovieSurprise = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "movie,Surprise");
}

var usersWhoClickedOnMovieSurprisePercentage = function (thisCampaignName) {
    return (usersWhoClickedOnMovieSurprise(thisCampaignName) / usersWhoClickedOnMovieCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnTvCategory = function (thisCampaignName) {
    return usersWhoClickedOnTvComedy(thisCampaignName)+usersWhoClickedOnTvDrama(thisCampaignName)+usersWhoClickedOnTvDocumentary(thisCampaignName)+usersWhoClickedOnTvSurprise(thisCampaignName);
}

var usersWhoClickedOnTvComedy = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "tvshow,Comedy");
}

var usersWhoClickedOnTvComedyPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnTvComedy(thisCampaignName) / usersWhoClickedOnTvCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnTvDrama = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "tvshow,Drama");
}

var usersWhoClickedOnTvDramaPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnTvDrama(thisCampaignName) / usersWhoClickedOnTvCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnTvDocumentary = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "tvshow,Documentary");
}

var usersWhoClickedOnTvDocumentaryPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnTvDocumentary(thisCampaignName) / usersWhoClickedOnTvCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnTvSurprise = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "tvshow,Surprise");
}

var usersWhoClickedOnTvSurprisePercentage = function (thisCampaignName) {
    return (usersWhoClickedOnTvSurprise(thisCampaignName) /usersWhoClickedOnTvCategory(thisCampaignName) * 100).toFixed(2);
}

var mostPopularTitles = function (thisCampaignName) {
    var messagesArr = Tracks.find({campaignName: thisCampaignName}).fetch().map(function (x) {
        return x.message;
    });

    var popularTitles = {};

    for(var i = 0; i < messagesArr.length; i++) {
        var startIndex = messagesArr[i].indexOf("Rm_");
        if(startIndex > -1) {
            var endIndex = messagesArr[i].indexOf(",", startIndex);
            var title;
            if (endIndex > -1)
                title = messagesArr[i].substring(startIndex+3, endIndex);
            else
                title = messagesArr[i].substring(startIndex+3);
            var value = popularTitles[title];
            if(isNaN(value))
                value = 0;
            popularTitles[title] = ++value;
        }
    }

    popularTitles = getSortedTitles(popularTitles);

    var tenPopularTitles = [];
    for (var i=9; i>=0; i--)
        tenPopularTitles.push(popularTitles[popularTitles.length-i-1]);

    return tenPopularTitles;
}

function getSortedTitles(obj) {
    var keys = []; for(var key in obj) keys.push(key);
    return keys.sort(function(a,b){return obj[a]-obj[b]});
}





this.CampaignsDetailsController = RouteController.extend({
	template: "CampaignsDetails",
	

	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady())
		{
			//Meteor.Spinner().stop();
			console.log("ready..."); this.render();
		}
		else
		{
			console.log("loading..."); this.render("loading");/*this.render();*/
			//Meteor.Spinner().spin();
		}

		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		

		var subs = [
			Meteor.subscribe("campaign", this.params.campaignId),
			Meteor.subscribe("tracks"),
			Meteor.subscribe("proxytracks")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		

		return {
			params: this.params || {},
			campaign: Campaigns.findOne( {_id:this.params.campaignId}, {} ),
			tracks: Tracks.find({}, {}),
			proxytracks: Tracks.find({}, {}),

		};
		/*DATA_FUNCTION*/
	},

	onAfterAction: function() {
		
	}
});